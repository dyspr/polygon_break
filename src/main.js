var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var initSize = 0.3
var dimension = 3
var numOfVertices = 4
var vertices = [[], []]
var nodes = []
for (var i = 0; i < dimension; i++) {
  for (var j = 0; j < dimension; j++) {
    nodes.push([i, j])
  }
}
var frame = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  nodes = shuffle(nodes)
  vertices[0] = nodes.splice(0, numOfVertices)
  vertices[1] = nodes.splice(0, numOfVertices)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var j = 0; j < vertices.length; j++) {
    fill(255)
    noStroke()
    beginShape()
    for (var i = 0; i < vertices[j].length; i++) {
      vertex(windowWidth * 0.5 + boardSize * initSize * (vertices[j][i][0] - Math.floor(dimension * 0.5)), windowHeight * 0.5 + boardSize * initSize * (vertices[j][i][1] - Math.floor(dimension * 0.5)))
    }
    endShape()
  }

  frame += deltaTime * 0.01
  if (frame > 1) {
    frame = 0
    var rand = Math.random()
    if (rand >= 0.5) {
      vertices = [[], []]
      for (var i = 0; i < dimension; i++) {
        for (var j = 0; j < dimension; j++) {
          nodes.push([i, j])
        }
      }
      nodes = shuffle(nodes)
      vertices[0] = nodes.splice(0, numOfVertices)
      vertices[1] = nodes.splice(0, numOfVertices)
    } else {
      vertices[0] = shuffle(vertices[0])
      vertices[1] = shuffle(vertices[1])
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
